dragora-handbook
================

This is the Handbook for the Dragora GNU/Linux-Libre project.

It contains the official instructions for the installation and use of the
system, as well as the guides included by the project for complement the
documentation in general.

Requirements
------------

- GNU Make

- GNU Texinfo

Installation
------------

To generate the necessary files, you have to type:

    $ make

This will produce the manual in various formats and in each available language,
saving its output in the prefixes of each language, by default "en" for
English.  If you want to output in a particular (available) language/format,
please check the Makefile for other targets.

Contact
-------

  The Dragora home page can be found at https://www.dragora.org.
Send bug reports or suggestions to <dragora-users@nongnu.org>.
