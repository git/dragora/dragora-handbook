# Makefile for the Dragora Handbook.
#
# Copyright (c) 2020, 2022, 2023 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

objects		= en/dragora-handbook-content.texi en/dragora-handbook-header.texi \
                  en/dragora-handbook-menu.texi en/qi-content.texi common/fdl.texi

.PHONY: all en


all : en

en : en/dragora-handbook.info en/dragora-handbook.doc en/dragora-handbook.html en/dragora-handbook.txt

en/dragora-handbook.info : ${objects}
	makeinfo en/dragora-handbook.texi -o $@

en/dragora-handbook.doc : ${objects}
	makeinfo --docbook en/dragora-handbook.texi -o $@
en/dragora-handbook.html : ${objects} common/handbook.css
	makeinfo --html --no-split \
         --css-include=common/handbook.css \
          en/dragora-handbook.texi -o $@
	gzip -f -k -9 en/dragora-handbook.html

en/dragora-handbook.txt : ${objects}
	makeinfo --plaintext en/dragora-handbook.texi > $@
	gzip -f -k -9 en/dragora-handbook.txt

clean :
	rm -f \
	 en/dragora-handbook.info \
	 en/dragora-handbook.doc \
	 en/dragora-handbook.html \
	 en/dragora-handbook.html.gz \
	 en/dragora-handbook.txt \
	 en/dragora-handbook.txt.gz
